import psutil

from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    cpu = psutil.cpu_percent(interval=1)
    mem = psutil.virtual_memory()
    memfree = psutil.virtual_memory().free
    disk = psutil.disk_usage('/')
    counter = psutil.disk_io_counters(perdisk=False, nowrap=True) 
    return render_template('index.html', mem = mem, memfree = memfree, disk = disk, counter = counter, cpu= cpu) 


if __name__ == "__main__":
    app.run(debug=True, port=5000)